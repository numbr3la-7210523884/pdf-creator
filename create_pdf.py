class PdfBodyObject:
    def __init__(self, obj_number = 0, content = ""):
        self.obj_number = obj_number
        self.content = content


    def __init__(self, obj_number = 0, body = []):
        self.obj_number = obj_number
        self.content = "\r\n".join(body)


    def __str__(self):
        result = str(self.obj_number) + " 0 obj\r\n<<\r\n"
        result += self.content
        result += "\r\n>>\r\nendobj\r\n"
        return result


    def add_to_content(self, extra_content):
        self.content += "\r\n" + extra_content




class PdfPage(PdfBodyObject):
    def __init__(self, width, height, parent_obj_number = 0, obj_number = 0, body = []):
        self.parent_pdf = None
        self.parent_obj_number = parent_obj_number
        self.obj_number = obj_number
        self.content = "\r\n".join(body)
        self.width = width
        self.height = height
        self.page_content = []


    def generate_content(self):
        page_elements_str = " 0 R ".join(map(str, self.page_content)) + " 0 R"

        self.content +=  "/Type /Page\r\n" \
            "/Parent " + str(self.parent_obj_number) + " 0 R\r\n" \
            "/MediaBox [0 0 " + str(self.width) + " " + str(self.height) + "]\r\n" \
            "/Contents [" + page_elements_str + "]"


    def __str__(self):
        self.generate_content()
        result = str(self.obj_number) + " 0 obj\r\n<<\r\n"
        result += self.content
        result += "\r\n>>\r\nendobj\r\n"
        return result

    
    def add_text(self, text, y, x, font_size=10, font_object_name="F1"):
        font_size_str = str(font_size)
        num_of_zeroes = 4 - len(font_size_str)
        font_size_str = str("0" * num_of_zeroes) + font_size_str
        
        text_obj = PdfBodyObject()
        content = "BT\r\n" \
        "/" + font_object_name + " " + font_size_str + " Tf\r\n" + \
        str(y) + " " + str(x) +  " Td\r\n" \
        "( " + text + " ) Tj\r\n" \
        "ET"
        text_obj.content = PdfFile.generate_str_stream(content)
        self.parent_pdf.add_obj(text_obj)
        self.page_content.append(text_obj.obj_number)




class XRefTable:
    def __init__(self):
        self.xref_objs_offset = []
        self.current_offset = 0

    def add_to_xref_table(self, object_str):
        self.xref_objs_offset.append(self.current_offset)
        self.current_offset = self.xref_objs_offset[-1] + len(object_str)

    def return_xref(self):
        result = "xref\r\n0 "
        result += str(len(self.xref_objs_offset) + 1) 
        result += "\r\n0000000000 65535 f\r\n"

        for offset in self.xref_objs_offset:
            num_of_zeroes = 10 - len(str(offset))
            zeros_str = "0" * num_of_zeroes
            result += zeros_str + str(offset) + " 00000 n\r\n" 

        return result




class PdfFile:
    def __init__(self):
        self.header = "%PDF-1.3\r\n%âăĎÓ\r\n"
        self.root_obj = self.return_root_obj()
        self.outlines_info_obj = self.return_outlines_obj()
        self.pages_object_num = []
        self.creator_info_obj = self.return_creator_info_obj()
        self.number_default_objects = 5
        self.body = []
        self.trailer = ""
        self.x_ref_table = XRefTable()
        self.pages_container_obj_num = 3


    def return_root_obj(self):
        obj_body = ["/Type /Catalog", "/Outlines 2 0 R", "/Pages 3 0 R"]
        return PdfBodyObject(1, obj_body)


    def return_outlines_obj(self):
        obj_body = ["/Type /Outlines", "/Count 0"]
        return PdfBodyObject(2, obj_body)


    def return_pages_container_obj(self):
        pages_kids = ""
        if len(self.pages_object_num) != 0:
            pages_kids = " 0 R ".join(map(str, self.pages_object_num)) + " 0 R"
        obj_body = ["/Type /Pages", "/Kids [" + pages_kids + "]", "/Count " + str(len(self.pages_object_num))]
        return PdfBodyObject(self.pages_container_obj_num, obj_body)


    def return_creator_info_obj(self):
        obj_body = ["/Creator (Test)", "/Producer (Test)", "/CreationDate (D:20221001000000)"]
        return PdfBodyObject(4, obj_body)


    def return_default_font_obj(self):
        obj_body = ["/Type /Font", "/Subtype /Type1", "/Name /F1",
                "/BaseFont /Helvetica", "/Encoding /WinAnsiEncoding"]
        return PdfBodyObject(5, obj_body)


    def return_trailer(self):
        trailer = "trailer\r\n<<\r\n" \
            "/Size " + str(len(self.body) + self.number_default_objects + 1) + "\r\n" \
            "/Root 1 0 R\r\n" \
            ">>\r\n"
        return trailer


    def add_obj(self, obj):
        self.body.append(obj)
        if obj.obj_number == 0:
            obj.obj_number = len(self.body) + self.number_default_objects
        

    def add_page(self, page):
        self.add_obj(page)
        self.pages_object_num.append(page.obj_number)
        page.parent_pdf = self
        

    def generate_pdf_content(self):
        # header
        result = self.header
        self.x_ref_table.current_offset = len(self.header)
        
        default_objects = [self.root_obj,
                            self.outlines_info_obj,
                            self.return_pages_container_obj(),
                            self.return_default_font_obj(),
                            self.creator_info_obj]

        # default objects
        for default_obj in default_objects:
            default_object_str = default_obj.__str__()
            result += default_object_str
            self.x_ref_table.add_to_xref_table(default_object_str)

        # body objects
        for body_obj in self.body:
            body_obj_str = body_obj.__str__()
            result += body_obj_str
            self.x_ref_table.add_to_xref_table(body_obj_str)
            
        xref_table_offset = self.x_ref_table.current_offset
        result += self.x_ref_table.return_xref()

        # trailer
        result += self.return_trailer() + "\r\n"

        # startxref
        result += "startxref\r\n" + str(xref_table_offset) + "\r\n%%EOF\r\n"
        
        return str.encode(result)


    @staticmethod
    def generate_str_stream(content):
        header = "<< /Length " + str(len(content)) + " >>\r\nstream\r\n"
        end = "\r\nendstream"
        result = header + content + end
        return result





def main():

    text = ["Hello world!", "TEST TEST TEST"]
    
    filename = "file.pdf"
    pdf = PdfFile()

    page = PdfPage(612.0, 792.0, pdf.pages_container_obj_num)
    pdf.add_page(page)

    page.add_text(text[0], 50, 700)
    page.add_text(text[1], 50, 690)


    file = open(filename, "wb")
    file.write(pdf.generate_pdf_content())
    file.close()




if __name__ == "__main__":
    main()